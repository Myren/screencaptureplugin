//
//  UnityPluginUtility.cpp
//  screenCapturePlugin
//
//  Created by Myren Yang on 21/10/13.
//
//

#include "UnityPluginUtility.h"
#include <string.h>
#include <stdlib.h>

// need to return a copy of the c string so that Unity handles the memory and gets a valid value.

char* cStringCopy(const char* string)
{
    if (string == 0)
        return 0;
    
    char* res = (char*)malloc(strlen(string) + 1);
    strcpy(res, string);
    
    return res;
}
