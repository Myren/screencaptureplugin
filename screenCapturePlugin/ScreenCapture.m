//
//  ScreenCapture.m
//  screenCapturePlugin
//
//  Created by Myren Yang on 13/05/13.
//  Copyright (c) 2013 __MyCompanyName__. All rights reserved.
//

#import "ScreenCapture.h"


// for plugin testing
const char* PrintHello(){
	return "Hello World";
}

int PrintANumber(){
	return 5;
}

int AddTwoIntegers(int a, int b) {
	return a + b;
}

float AddTwoFloats(float a, float b) {
	return a + b;
}
void testByteArray(unsigned char* array, int size) 
{
    for (int i = 0; i < size; i++)
    {
        array[i] = (unsigned char) (size - array[i]);
    }
}
// end plugin testing

static NSString *errorMessage = @"";
static CGImageRef lastCapturedImage = NULL;

CGImageRef createResizedCGImage(CGImageRef sourceImage, int width, int height);
int convertFromCGImageToJPEGData(CGImageRef image, unsigned char* jpeg);
int convertFromCGIMageTORawData(CGImageRef image, unsigned char* data);

int resizeCGImageAndReturnARGB(CGImageRef sourceImage, unsigned char* pixelData, int width, int height);
int resizeCGImageAndReturnBGR(CGImageRef sourceImage, unsigned char* pixelData, int width, int height);
CGContextRef CreateARGBBitmapContextWithSize(CGImageRef sourceImage, int width, int height);
void saveCGContextToJPGFile(CGContextRef context, NSString* filename);




int convertFromCGIMageTORawData(CGImageRef image, unsigned char* data)
{
    int length = 0;
    
    /* get pixels */
    //CGDataProviderRef provider = CGImageGetDataProvider(image);
    //CFDataRef dataRef = CGDataProviderCopyData(provider);
    //uint8_t* pixels = (uint8_t*)CFDataGetBytePtr(dataRef);
    //data = (uint8_t*)CFDataGetBytePtr(dataRef);
    //*length = CFDataGetLength(dataRef);
    
    return length;
}

int convertFromCGImageToJPEGData(CGImageRef image, unsigned char* jpeg)
{
    int length = 0;
    
    // Create a bitmap rep from the image...
    NSBitmapImageRep *bitmapRep = [[NSBitmapImageRep alloc] initWithCGImage:image];
    NSData* data = [bitmapRep representationUsingType:NSJPEGFileType properties:[NSDictionary dictionaryWithObject:[NSDecimalNumber numberWithFloat:1.0] forKey:NSImageCompressionFactor]];
    
    [data writeToFile:@"Assets/picFromPlugin.jpg" atomically:YES];  // for testing
    
    //jpeg = (unsigned char*) data.bytes;
    length = data.length;
    memcpy(jpeg, data.bytes, length);
    
    [bitmapRep release];
    
    return length;
}

CGImageRef createResizedCGImage(CGImageRef sourceImage, int width, int height)
{
    CGImageRef destImage;
    
    CGContextRef context = CreateARGBBitmapContextWithSize(sourceImage, width, height);
    
    destImage = CGBitmapContextCreateImage( context );
    
    // When finished, release the context
    CGContextRelease(context);
     
    return destImage;
}




void saveCGContextToJPGFile(CGContextRef context, NSString* filename)
{
    CGImageRef testImage = CGBitmapContextCreateImage( context );
    NSBitmapImageRep *bitmapRep = [[NSBitmapImageRep alloc] initWithCGImage:testImage];
    NSData* dt = [bitmapRep representationUsingType:NSJPEGFileType properties:[NSDictionary dictionaryWithObject:[NSDecimalNumber numberWithFloat:1.0] forKey:NSImageCompressionFactor]];
    [dt writeToFile:filename atomically:YES];
    
    [bitmapRep release];
    CGImageRelease(testImage);
}

int resizeCGImageAndReturnARGB(CGImageRef sourceImage, unsigned char* pixelData, int width, int height)
{
    int length = width*height*4;
    
    CGContextRef context = CreateARGBBitmapContextWithSize(sourceImage, width, height);
        
    unsigned char* data = CGBitmapContextGetData(context);
    if (data != NULL)
    {
        // **** You have a pointer to the image data ****
        // **** Do stuff with the data here ****
        //for (int i = 0, j = 0; i < bitmapByteCount && j < length; i++)
        for (int i = 0; i < length; i++)
        {
            // Target.R = ((1 - Source.A) * BGColor.R) + (Source.A * Source.R)
            // 255 + (a*(r-255))
            //pixelData[i*3] = (1 - data[i*4]/255.0) + (data[i*4]/255.0*data[i*4+1]/255.0);
            //pixelData[i*3] = 255 - (data[i*4]*(255-data[i*4+1]));
            //pixelData[i*3+1] = 255 - (data[i*4]*(255-data[i*4+2]));
            //pixelData[i*3+2] = 255 - (data[i*4]*(255-data[i*4+3]));            
            pixelData[i] = data[i];
        }
    }
    
    // output file for testing
    //saveCGContextToJPGFile(context, @"Assets/picFromPlugin.jpg");
    
    // When finished, release the context
    CGContextRelease(context);
    // Free image data memory for the context
    if (data)
    {
        free(data);
    }
    
    return length;
}

int resizeCGImageAndReturnBGR(CGImageRef sourceImage, unsigned char* pixelData, int width, int height)
{
    int length = 0;
    int p = 0;
    
    CGContextRef context = CreateARGBBitmapContextWithSize(sourceImage, width, height);
    
    unsigned char* data = CGBitmapContextGetData(context);
    if (data != NULL)
    {
        // **** You have a pointer to the image data ****
        // **** Do stuff with the data here ****
        //for (int i = 0, j = 0; i < bitmapByteCount && j < length; i++)
        for (p = 0; p < width*height; p++)
        {
            pixelData[p*3] = data[p*4+3];
            pixelData[p*3+1] = data[p*4+2];
            pixelData[p*3+2] = data[p*4+1];
        }
        length = p * 3;
    }
    
    // output file for testing
    //saveCGContextToJPGFile(context, @"Assets/picFromPlugin.jpg");
    
    // When finished, release the context
    CGContextRelease(context);
    // Free image data memory for the context
    if (data)
    {
        free(data);
    }
    
    return length;
}

int captureScreenReturnJPEG(unsigned char* imageBuffer, int imageWidth, int imageHeight)
{
    int size = 0;
    
    // take the screenshot
	CGImageRef screenShot = CGWindowListCreateImage(CGRectInfinite, kCGWindowListOptionOnScreenOnly, kCGNullWindowID, kCGWindowImageDefault);
    
    CGImageRef newScreenShot = createResizedCGImage(screenShot, imageWidth, imageHeight);
    CGImageRelease(screenShot);
    
    size = convertFromCGImageToJPEGData(newScreenShot, imageBuffer);
    
    //size = convertFromCGIMageTORawData(newScreenShot, imageBuffer);
    
    
    if (lastCapturedImage)
        CGImageRelease(lastCapturedImage);
    lastCapturedImage = newScreenShot;
    
    if (size)
        return size;
    return 0;
}

int captureScreenReturnARGB(unsigned char* imageBuffer, int imageWidth, int imageHeight)
{
    int size = 0;
    
    // take the screenshot
	CGImageRef screenShot = CGWindowListCreateImage(CGRectInfinite, kCGWindowListOptionOnScreenOnly, kCGNullWindowID, kCGWindowImageDefault);
    
    size = resizeCGImageAndReturnARGB(screenShot, imageBuffer, imageWidth, imageHeight);

    if (lastCapturedImage)
        CGImageRelease(lastCapturedImage);
    lastCapturedImage = screenShot;
    
    if (size)
        return size;
    return 0;
}

int captureScreenReturnBGR(unsigned char* imageBuffer, int imageWidth, int imageHeight)
{
    int size = 0;
    
    // take the screenshot
	CGImageRef screenShot = CGWindowListCreateImage(CGRectInfinite, kCGWindowListOptionOnScreenOnly, kCGNullWindowID, kCGWindowImageDefault);
    
    size = resizeCGImageAndReturnBGR(screenShot, imageBuffer, imageWidth, imageHeight);
    
    if (lastCapturedImage)
        CGImageRelease(lastCapturedImage);
    lastCapturedImage = screenShot;

    if (size)
        return size;
    return 0;
}

void captureScreenReturnSize(int* pImageWidth, int* pImageHeight)
{
    // take the screenshot
    CGImageRef screenShot = CGWindowListCreateImage(CGRectInfinite, kCGWindowListOptionOnScreenOnly, kCGNullWindowID, kCGWindowImageDefault);
    
    *pImageWidth = CGImageGetWidth(screenShot);
    *pImageHeight = CGImageGetHeight(screenShot);
    
    if (lastCapturedImage)
        CGImageRelease(lastCapturedImage);
    lastCapturedImage = screenShot;
}




CGContextRef CreateARGBBitmapContextWithSize(CGImageRef sourceImage, int width, int height)
{
    // Declare the number of bytes per row. Each pixel in the bitmap in this
    // example is represented by 4 bytes; 8 bits each of red, green, blue, and
    // alpha.
    int bitmapBytesPerRow   = (width * 4);
    int bitmapByteCount     = (bitmapBytesPerRow * height);
    
    // Use the generic RGB color space.
    CGColorSpaceRef colorSpace = CGColorSpaceCreateWithName(kCGColorSpaceGenericRGB);
    if (colorSpace == NULL)
    {
        fprintf(stderr, "Error allocating color space\n");
        errorMessage = @"Error allocating color space";
        return NULL;
    }
    
    // Allocate memory for image data. This is the destination in memory
    // where any drawing to the bitmap context will be rendered.
    void* bitmapData = malloc( bitmapByteCount );
    if (bitmapData == NULL)
    {
        fprintf (stderr, "Memory not allocated!");
        CGColorSpaceRelease( colorSpace );
        errorMessage = @"Memory not allocated!";
        return NULL;
    }
    
    // Create the bitmap context. We want pre-multiplied ARGB, 8-bits 
    // per component. Regardless of what the source image format is 
    // (CMYK, Grayscale, and so on) it will be converted over to the format
    // specified here by CGBitmapContextCreate.
    CGContextRef context = CGBitmapContextCreate (bitmapData,
                                                  width,
                                                  height,
                                                  8,      // bits per component
                                                  bitmapBytesPerRow,
                                                  colorSpace,
                                                  //kCGImageAlphaNone);/* RGB, no way */
                                                  kCGImageAlphaPremultipliedFirst);/*ARGB*/
    if (context == NULL)
    {
        free (bitmapData);
        fprintf (stderr, "Context not created!");
        errorMessage = @"Context not created!";
    }
    
    // Make sure and release colorspace before returning
    CGColorSpaceRelease( colorSpace );
    
    // Get image width, height. We'll use the entire image.
    //int oldWidth = CGImageGetWidth(sourceImage);
    //int oldHeight = CGImageGetHeight(sourceImage);
    CGRect rect = {{0,0},{width,height}};
    
    // Draw the image to the bitmap context. Once we draw, the memory 
    // allocated for the context for rendering will then contain the 
    // raw image data in the specified color space.
    CGContextDrawImage(context, rect, sourceImage);
    
    return context;    
}



const char* getErrorMessage(void)
{
    NSString * temp = errorMessage;
    errorMessage = @"";
    return [temp cStringUsingEncoding:NSASCIIStringEncoding];
}


// for window capture
static NSMutableArray * prunedWindowList = nil;

bool validWindowID(CGWindowID windowID);

int createSingleWindowReturnBGR (CGWindowID windowID, unsigned char* imageBuffer, int imageWidth, int imageHeight)
{
    int size = 0;
    
    if (!validWindowID(windowID))
    {
        errorMessage = @"The required window is not readable: it could be closed or minimized";
        return 0;
    }
    
    CGWindowImageOption imageOptions;
    imageOptions = kCGWindowImageDefault;
    //imageOptions = imageOptions | kCGWindowImageBoundsIgnoreFraming;
    imageOptions = imageOptions | kCGWindowImageShouldBeOpaque;
    //imageOptions = imageOptions | kCGWindowImageOnlyShadows;
    
	// Create an image from the passed in windowID with the single window option selected by the user.
	CGImageRef windowImage = CGWindowListCreateImage(CGRectNull, kCGWindowListOptionIncludingWindow, windowID, imageOptions);
    
    size = resizeCGImageAndReturnBGR(windowImage, imageBuffer, imageWidth, imageHeight);

    if (lastCapturedImage)
        CGImageRelease(lastCapturedImage);
    lastCapturedImage = windowImage;
    
    if (size)
        return size;
    return 0;
}

int createSingleWindowReturnSize(CGWindowID windowID, int* pImageWidth, int* pImageHeight)
{
    if (!validWindowID(windowID))
    {
        errorMessage = @"The required window is not readable: it could be closed or minimized";
        return 0;
    }
    
    CGWindowImageOption imageOptions;
    imageOptions = kCGWindowImageDefault;
    //imageOptions = imageOptions | kCGWindowImageBoundsIgnoreFraming;
    imageOptions = imageOptions | kCGWindowImageShouldBeOpaque;
    //imageOptions = imageOptions | kCGWindowImageOnlyShadows;
    
    // Create an image from the passed in windowID with the single window option selected by the user.
    CGImageRef windowImage = CGWindowListCreateImage(CGRectNull, kCGWindowListOptionIncludingWindow, windowID, imageOptions);
    
    *pImageWidth = CGImageGetWidth(windowImage);
    *pImageHeight = CGImageGetHeight(windowImage);
    
    if (lastCapturedImage)
        CGImageRelease(lastCapturedImage);
    lastCapturedImage = windowImage;

    return 1;
}


typedef struct
{
	// Where to add window information
	NSMutableArray * outputArray;
	// Tracks the index of the window when first inserted
	// so that we can always request that the windows be drawn in order.
	int order;
} WindowListApplierData;

static NSString *kWindowName = @"windowName";       // Window name (may be unknown)
static NSString *kAppNameKey = @"applicationName";	// Application Name & PID
static NSString *kWindowOriginKey = @"windowOrigin";	// Window Origin as a string
static NSString *kWindowSizeKey = @"windowSize";		// Window Size as a string
static NSString *kWindowIDKey = @"windowID";			// Window ID
static NSString *kWindowLevelKey = @"windowLevel";	// Window Level
static NSString *kWindowOrderKey = @"windowOrder";	// The overall front-to-back ordering of the windows as returned by the window server

void WindowListApplierFunction(const void *inputDictionary, void *context);
void WindowListApplierFunction(const void *inputDictionary, void *context)
{
	NSDictionary *entry = (NSDictionary*)inputDictionary;
	WindowListApplierData *data = (WindowListApplierData*)context;
    
	// Filter: only get the window with level key is 0
    if ([[entry objectForKey:(id)kCGWindowLayer] intValue] != 0)
        return;

	// The flags that we pass to CGWindowListCopyWindowInfo will automatically filter out most undesirable windows.
	// However, it is possible that we will get back a window that we cannot read from, so we'll filter those out manually.
	int sharingState = [[entry objectForKey:(id)kCGWindowSharingState] intValue];
	if(sharingState != kCGWindowSharingNone)
	{
		NSMutableDictionary *outputEntry = [NSMutableDictionary dictionary];
		
		// Grab the application name, but since it's optional we need to check before we can use it.
		NSString *applicationName = [entry objectForKey:(id)kCGWindowOwnerName];
		if(applicationName != NULL)
		{
			// PID is required so we assume it's present.
			NSString *nameAndPID = [NSString stringWithFormat:@"%@ (%@)", applicationName, [entry objectForKey:(id)kCGWindowOwnerPID]];
			[outputEntry setObject:nameAndPID forKey:kAppNameKey];
		}
		else
		{
			// The application name was not provided, so we use a fake application name to designate this.
			// PID is required so we assume it's present.
			NSString *nameAndPID = [NSString stringWithFormat:@"((unknown)) (%@)", [entry objectForKey:(id)kCGWindowOwnerPID]];
			[outputEntry setObject:nameAndPID forKey:kAppNameKey];
		}
        
        // Grab the window name, it may be null
        NSString *windowName = [entry objectForKey:(id)kCGWindowName];
        if (windowName == NULL)
        {
            windowName = @"NoWindowName";
        }
        [outputEntry setObject:windowName forKey:kWindowName];
		
		// Grab the Window Bounds, it's a dictionary in the array, but we want to display it as a string
		CGRect bounds;
		CGRectMakeWithDictionaryRepresentation((CFDictionaryRef)[entry objectForKey:(id)kCGWindowBounds], &bounds);
		NSString *originString = [NSString stringWithFormat:@"%.0f/%.0f", bounds.origin.x, bounds.origin.y];
		[outputEntry setObject:originString forKey:kWindowOriginKey];
		NSString *sizeString = [NSString stringWithFormat:@"%.0f*%.0f", bounds.size.width, bounds.size.height];
		[outputEntry setObject:sizeString forKey:kWindowSizeKey];
		
		// Grab the Window ID & Window Level. Both are required, so just copy from one to the other
		[outputEntry setObject:[entry objectForKey:(id)kCGWindowNumber] forKey:kWindowIDKey];
		[outputEntry setObject:[entry objectForKey:(id)kCGWindowLayer] forKey:kWindowLevelKey];
		
		// Finally, we are passed the windows in order from front to back by the window server
		// Should the user sort the window list we want to retain that order so that screen shots
		// look correct no matter what selection they make, or what order the items are in. We do this
		// by maintaining a window order key that we'll apply later.
		[outputEntry setObject:[NSNumber numberWithInt:data->order] forKey:kWindowOrderKey];
		data->order++;
		
                
		[data->outputArray addObject:outputEntry];
	}
}

int updateWindowList()
{
    CGWindowListOption listOptions;
    
    listOptions = kCGWindowListOptionAll;
    listOptions = listOptions | kCGWindowListOptionOnScreenOnly;
    listOptions = listOptions | kCGWindowListExcludeDesktopElements;
    
	// Ask the window server for the list of windows.
	CFArrayRef windowList = CGWindowListCopyWindowInfo(listOptions, kCGNullWindowID);
	
	// Copy the returned list, further pruned, to another list. This also adds some bookkeeping
	// information to the list as well as 
    if (prunedWindowList)
        releaseWindowList();
	prunedWindowList = [[NSMutableArray alloc] init];
	WindowListApplierData data = {prunedWindowList, 0};
	CFArrayApplyFunction(windowList, CFRangeMake(0, CFArrayGetCount(windowList)), &WindowListApplierFunction, &data);
	CFRelease(windowList);
    
    return [prunedWindowList count];
}
void releaseWindowList(void)
{
    if (prunedWindowList)
    {
        [prunedWindowList release];
        prunedWindowList = nil;
    }
}
bool validWindowID(CGWindowID windowID)
{
    bool valid = false;
    CGWindowListOption listOptions;
    
    listOptions = kCGWindowListOptionAll;
    listOptions = listOptions | kCGWindowListOptionOnScreenOnly;
    listOptions = listOptions | kCGWindowListExcludeDesktopElements;

    CFArrayRef allWindowIDs = CGWindowListCreate(listOptions, kCGNullWindowID);
        
    valid = CFArrayContainsValue(allWindowIDs, CFRangeMake(0, CFArrayGetCount(allWindowIDs)), (void*)(uintptr_t)windowID);
    
    CFRelease(allWindowIDs);
    return valid;
}

const char* getProcessNameAndPID(int index)
{
    if (index < 0 || index >= [prunedWindowList count])
    {
        errorMessage = @"Invalid index";
        return "Invalid index";
    }
    
    NSString* nameAndPID = [[prunedWindowList objectAtIndex:index] objectForKey:kAppNameKey];
    
    //return [nameAndPID cStringUsingEncoding:NSASCIIStringEncoding];
    return cStringCopy([nameAndPID UTF8String]);
}
CGWindowID getWindowID(int index)
{
    if (index < 0 || index >= [prunedWindowList count])
    {
        errorMessage = @"Invalid index";
        return 0;
    }
    
    //NSString* windowID = [[prunedWindowList objectAtIndex:index] objectForKey:kWindowIDKey];
    CGWindowID windowID = [[[prunedWindowList objectAtIndex:index] objectForKey:kWindowIDKey] unsignedIntValue];

    //return [windowID cStringUsingEncoding:NSASCIIStringEncoding];
    return windowID;
}
const char* getWindowName(int index)
{
    if (index < 0 || index >= [prunedWindowList count])
    {
        errorMessage = @"Invalid index";
        return "Invalid index";
    }
    
    NSString* windowName = [[prunedWindowList objectAtIndex:index] objectForKey:kWindowName];
    
    //return [windowName cStringUsingEncoding:NSASCIIStringEncoding];
    return cStringCopy([windowName UTF8String]);
}

// for thumbnail or other sizes of images
int resizeCapturedImageReturnBGR (unsigned char* imageBuffer, int imageWidth, int imageHeight)
{
    int size = 0;
    
    if (!lastCapturedImage)
    {
        errorMessage = @"Error to fetch last captured image";
        return 0;
    }
        
    size = resizeCGImageAndReturnBGR(lastCapturedImage, imageBuffer, imageWidth, imageHeight);
    
    CGImageRelease(lastCapturedImage);
    lastCapturedImage = NULL;
    
    return size;
}

int getCapturedImageData (unsigned char* imageBuffer)
{
    int size = 0;
    
    if (!lastCapturedImage)
    {
        errorMessage = @"Error to fetch last captured image";
        return 0;
    }
    
    int imageWidth = CGImageGetWidth(lastCapturedImage);
    int imageHeight = CGImageGetHeight(lastCapturedImage);
    
    size = resizeCGImageAndReturnBGR(lastCapturedImage, imageBuffer, imageWidth, imageHeight);
    
    return size;
}


@implementation ScreenCapture

@end
