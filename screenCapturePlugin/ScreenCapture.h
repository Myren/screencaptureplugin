//
//  ScreenCapture.h
//  screenCapturePlugin
//
//  Created by Myren Yang on 13/05/13.
//  Copyright (c) 2013 __MyCompanyName__. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import "UnityPluginUtility.h"

//extern "C" {

// for test
const char* PrintHello (void);
int PrintANumber (void);
int AddTwoIntegers(int, int);
float AddTwoFloats(float, float);
void testByteArray(unsigned char* array, int size);


const char* getErrorMessage(void);

// for screen capture
int captureScreenReturnJPEG(unsigned char* imageBuffer, int imageWidth, int imageHeight);
int captureScreenReturnARGB(unsigned char* imageBuffer, int imageWidth, int imageHeight);
int captureScreenReturnBGR(unsigned char* imageBuffer, int imageWidth, int imageHeight);
void captureScreenReturnSize(int* pImageWidth, int* pImageHeight);

// for window capture
int createSingleWindowReturnBGR (uint32_t windowID, unsigned char* imageBuffer, int imageWidth, int imageHeight);
int createSingleWindowReturnSize(CGWindowID windowID, int* pImageWidth, int* pImageHeight);
int updateWindowList(void);
void releaseWindowList(void);
const char* getProcessNameAndPID(int index);
uint32_t getWindowID(int index);
const char* getWindowName(int index);

// for thumbnail or other sizes of images
int resizeCapturedImageReturnBGR (unsigned char* imageBuffer, int imageWidth, int imageHeight);
int getCapturedImageData (unsigned char* imageBuffer);
//}

@interface ScreenCapture : NSObject

@end
